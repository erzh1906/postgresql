#------------------------------------------------------------------------------
# CONNECTIONS AND AUTHENTICATION
#------------------------------------------------------------------------------
listen_addresses                                = '*'
port                                            = 5432
max_connections                                 = {{ pg_max_connections }}
superuser_reserved_connections                  = 3
authentication_timeout                          = 30s
password_encryption                             = md5

#------------------------------------------------------------------------------
# RESOURCE USAGE (except WAL)
#------------------------------------------------------------------------------

# - Memory -

shared_buffers                                  = {{ pg_shared_buffers }}
huge_pages                                      = try
temp_buffers                                    = 8MB
max_prepared_transactions                       = 0
work_mem                                        = 64MB
maintenance_work_mem                            = 64MB
autovacuum_work_mem                             = -1
max_stack_depth                                 = 2MB
dynamic_shared_memory_type                      = posix

#------------------------------------------------------------------------------
# WRITE AHEAD LOG
#------------------------------------------------------------------------------

wal_level                                       = replica
fsync                                           = on
synchronous_commit                              = local
wal_sync_method                                 = fsync
full_page_writes                                = on
wal_compression                                 = off
wal_log_hints                                   = off
wal_buffers                                     = -1
wal_writer_delay                                = 200ms
wal_writer_flush_after                          = 1MB
commit_delay                                    = 0
commit_siblings                                 = 5

# - Archiving -

archive_mode                                    = on
archive_command                                 = 'cp %p /var/lib/pgsql/11/archive/%f'
archive_timeout                                 = 0

#------------------------------------------------------------------------------
# REPLICATION
#------------------------------------------------------------------------------

# - Sending Server(s) -

max_wal_senders                                 = 50
wal_keep_segments                               = 10
wal_sender_timeout                              = 60s
max_replication_slots                           = 50

#------------------------------------------------------------------------------
# DISK
#------------------------------------------------------------------------------

shared_preload_libraries                        = 'pg_stat_statements'
pg_stat_statements.max                          = 10000
pg_stat_statements.track                        = all

#------------------------------------------------------------------------------
# ERROR REPORTING AND LOGGING
#------------------------------------------------------------------------------

log_destination                                 = 'syslog'
syslog_facility                                 = 'LOCAL0'
syslog_ident                                    = 'postgres'
syslog_sequence_numbers                         = on
syslog_split_messages                           = on
client_min_messages                             = notice
log_min_messages                                = warning
log_min_error_statement                         = error
log_min_duration_statement                      = -1
log_line_prefix                                 = '%m [%p] %r %u/%d %c '
