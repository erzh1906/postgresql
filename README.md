# Simple role for setting up standalone PostgreSQL server on CentOS 7

**Variables:**

  - `pg_max_connections:` max_connections parameter of PostgreSQL configuration 
  - `pg_shared_buffers:` shared_buffers parameter of PostgreSQL configuration
